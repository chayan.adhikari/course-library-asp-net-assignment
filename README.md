# Backend Assignment: Simple Course Library API

The assignment involves the creation of a course library REST JSON API using ASP.NET Core. Please use the following libraries and versions:

* ASP.NET 6.0

## Simple Course Library API

Create a CRUD API for a simple Course Library application. The API can be used to create authors and courses. Authors will have first name, last name, DOB, email id properties. An author can write one or more courses. Each course should have a title and description.

Via a REST API it must be possible to:

*   List all authors
*   Add a new author
*   Create courses for an author
*   List all courses for an author 
*   Get one course for an author by the course identifier
*   Update course for an author
*   Partially update course for an author using patch request
*   Delete course for an author
*   Create a batch of authors using a single endpoint 
*   Get a batch of authors corresponding to multiple author identifiers passed as parameter to an endpoint
*   Implement paging and sorting(by name for authors and by title for courses) for all endpoints that return multiple results
*   The author should receive an email with the course title everytime a course is added for him/her
 

User authentication is **NOT** required. The data doesn't need to be persisted, You can either use EF Core's in-memory database or a mock data repository that can hold data in a Dictionary/ConcurrentDictionary data structure. **However, please ensure that the mock data repository methods are asyncronous.**

This link gives an overview of adding test data and configuring in-memory database using EF Core: https://stormpath.com/blog/tutorial-entity-framework-core-in-memory-database-asp-net-core

Sending of the email can be mocked by simply printing the email id of the user in the console instead of using a SMTP server to send an actual email.

## How to work on the assessment

*   Create a Gitlab account and let the recruiter know so that access can be granted to the repository (https://www.youtube.com/watch?v=Qz_RowKsU50)
*   Clone this repository
*   Create a new feature branch from main using the naming format fb/<your_name>_courselibrary_assignment (https://www.tutorialspoint.com/gitlab/gitlab_create_branch.htm)
*   Create a Merge Request to the main branch (https://www.tutorialspoint.com/gitlab/gitlab_merge_requests.htm)
*   Checkout the feature branch in your local system  (git checkout fb/<your_name>_courselibrary_assignment)
*   Start working on the assignment
*   Please do periodic commits with meaningful commit messages and push to the feature branch
*   Please include a brief description how to run your solution
*   If you have any questions contact the corresponding recruiter

Please note that we don't accept solutions without periodic commits or if we are unable to execute the solution.